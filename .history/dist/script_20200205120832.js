$(document).ready(function() 
{
	$("#baconButton").click(function()
	{
		$.getJSON('https://baconipsum.com/api/?type=all-meat&paras=3&start-with-lorem=1&format=html', 
			{ 'type':'meat-and-filler', 'start-with-lorem':'1', 'paras':'3' }, 
			function(baconGoodness)
		{
			if (baconGoodness && baconGoodness.length > 0)
			{
				$("#baconIpsumOutput").html('');
				for (var i = 0; i < baconGoodness.length; i++)
					$("#baconIpsumOutput").append('<p>' + baconGoodness[i] + '</p>');
				$("#baconIpsumOutput").show();
			}
		});
	});
});