<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <title></title>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
       
         <a class="navbar-brand" href="#">
            <img src="examenwb/src/logo.png" width="100" height="60" class="d-inline-block align-top" alt="">
           </a>
        <div class="collapse navbar-collapse  nav-right" id="navbarNavAltMarkup">
          <div class=" navbar-nav ">
            <a class="nav-item nav-link active" href="#">HOME</a>
            <a class="nav-item nav-link" href="#">PAGES</a>
            <a class="nav-item nav-link" href="#">PORTOFOLIO</a>
            <a class="nav-item nav-link" href="#">BLOG</a>
            <a class="nav-item nav-link" href="#">POST FORMATS</a>
            <a class="nav-item nav-link" href="#">JOOMLA</a>
            <a class="nav-item nav-link" href="#">K2 BLOG</a>
           </div>
        </div>
      </nav>

      <!-- Welcome -->
      <section class="page-section" id="services">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h2 class="section-heading text-uppercase">WELCOME TO AT RESTAURANT</h2>
              <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
            </div>
          </div>
          <div class="row text-center">
            <div class="col-md-3">
              <span class="fa-stack fa-4x">
                <i class="fa fa-apple" style="font-size:48px;color:red"></i>
              </span>
              <h4 class="service-heading">BEST CUISINE</h4>
              <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
            </div>
            <div class="col-md-3">
              <span class="fa-stack fa-4x">
                <i class="fa fa-chrome" style="font-size:48px;color:red"></i>
              </span>
              <h4 class="service-heading">SPECIAL OFFERS</h4>
              <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
            </div>
            <div class="col-md-3">
              <span class="fa-stack fa-4x">
                <i class="fa fa-clock-o" style="font-size:48px;color:red"></i>
              </span>
              <h4 class="service-heading">GOOD REST</h4>
              <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
            </div>
            <div class="col-md-3">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-creative-commons" style="font-size:48px;color:red"></i>
                </span>
                <h4 class="service-heading">TIMINGS</h4>
                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
              </div>
          </div>
        </div>
      </section>

      <button type="button" class="btn btn-secondary" id="baconButton" >BACON</button>
     <div id="baconIpsumOutput"></div>

      <section class="page-section" id="services">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h2 class="section-heading text-uppercase">Latest WORK</h2>
              <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                ut enim ad minim veniam, quis nostrud exercitation ullamco..</h3>
            </div>

            <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "examenweb";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM produse";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "Nume:  " . $row["Nume"]. " Categorie :  " . $row["Categorie"]. " Imagine :  " . $row["Imagine"]. "<br>";
    }
} else {
    echo "0 results";
}
$conn->close();
?>
?>
         </div>
        </div>
      </section>

     




  
  <script src="./bundle.js"></script>
  <script src="./script.js"></script>
</body>
</html>